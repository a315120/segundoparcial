const supertest = require('supertest');
const app = require('../app');

var key="";
//sentencia
describe('Probar el sistema de autenticacion', ()=>{
  //casos de prueba => %50
  it('Deberia de obtener un login con usuario y contraseña correcto', (done)=>{
    supertest(app).post('/login')
    .send({'email':'correo@algo.com', 'password':'abcd1234'}) //login exitoso agregar usuario y pass
    .expect(200) // status HTTP code
    .end(function(err, res){
      key = res.body.obj;
      done();
    });
  });
});

describe('probar las rutas de los usuarios', ()=>{
  it('deberia de obtener la lista de usuarios', (done)=>{
    supertest(app).get('/users/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  });
});
